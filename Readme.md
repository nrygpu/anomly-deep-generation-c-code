Anomly Deep Generation C Code 

Deep Neural Network experimentations based on: https://becominghuman.ai/how-to-train-a-neural-network-to-code-by-itself-a432e8a120df and updated with the latest NVIDIA RTX 2080 Ti and longer DNN training times.

Test One: 12 hours   
Test Two: 48 hours  
Test Three: Approx 5 days

NOTE: "Anomly" is a play on the obvious word Anomaly and based on a potential website domain name for this project that I bought a few years ago